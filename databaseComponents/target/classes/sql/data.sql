-- Roles --
INSERT INTO ROLES VALUES (0, 'Пользователь ЛК')
INSERT INTO ROLES VALUES (1, 'Администратор')

-- Health categories --
INSERT INTO HEALTH_CATEGORIES VALUES (0, 'Специальная', 1, '2019-10-15 17:21:57')
INSERT INTO HEALTH_CATEGORIES VALUES (1, 'Основная', 1, '2019-11-03 18:44:33')

-- Specializations -
INSERT INTO SPECIALIZATIONS VALUES (1, 'Аэробика', 0, '../somepath')
INSERT INTO SPECIALIZATIONS VALUES (2, 'ОФП', 0, '../somepath')
INSERT INTO SPECIALIZATIONS VALUES (3, 'Спортивные игры', 1, '../somepath')
INSERT INTO SPECIALIZATIONS VALUES (4, 'Бокс', 1, '../somepath')
INSERT INTO SPECIALIZATIONS VALUES (5, 'Борьба', 1, '../somepath')
INSERT INTO SPECIALIZATIONS VALUES (6, 'Плавание', 1, '../somepath')
INSERT INTO SPECIALIZATIONS VALUES (7, 'Тяжёлая атлетика', 1, '../somepath')

-- Trainers --
INSERT INTO Trainers VALUES (1, 'Бушма', 'Татьяна', 'Валерьевна', 1, '../somepath')
INSERT INTO Trainers VALUES (2, 'Васильева', 'Валерия', 'Сергеевна', 2, '../somepath')
INSERT INTO Trainers VALUES (3, 'Иванов', 'Валерий', 'Геннадьевич', 3, '../somepath')
INSERT INTO Trainers VALUES (4, 'Спиридонова', 'Лариса', 'Евгеньевна', 3, '../somepath')
INSERT INTO Trainers VALUES (5, 'Трещёв', 'Андрей', 'Вячеславович', 7, '../somepath')
INSERT INTO Trainers VALUES (4, 'Шарнин', 'Николай', 'Павлович', 4, '../somepath')
INSERT INTO Trainers VALUES (5, 'Щадрин', 'Игорь', 'Николаевич', 3, '../somepath')


-- Schedules --
INSERT INTO Schedules VALUES (1, 0, 1, '12:00', '13:40')
INSERT INTO Schedules VALUES (2, 0, 2, '08:00', '09:40')
INSERT INTO Schedules VALUES (3, 1, 1, '14:00', '15:40')
INSERT INTO Schedules VALUES (4, 1, 4, '16:00', '17:40')
INSERT INTO Schedules VALUES (5, 1, 5, '10:00', '11:40')
INSERT INTO Schedules VALUES (6, 1, 3, '18:00', '19:40')
INSERT INTO Schedules VALUES (7, 0, 0, '12:00', '13:40')

-- Locations --
INSERT INTO Locations VALUES (1, 'Парголовская ул., 8', '../somepath')
INSERT INTO Locations VALUES (2, 'Политехническая ул., 27', '../somepath')
INSERT INTO Locations VALUES (3, 'Энгельса пр., 23', '../somepath')

-- Groups --
INSERT INTO Groups VALUES (1, 1, 24, 11, 'закрыта', 'К занятиям в группе приглашаются студенты...')
INSERT INTO Groups VALUES (2, 2, 30, 18, 'открыта', 'К занятиям в группе приглашаются студенты...')
INSERT INTO Groups VALUES (3, 2, 28, 2, 'закрыта', 'К занятиям в группе приглашаются студенты...')
INSERT INTO Groups VALUES (4, 5, 30, 14, 'открыта', 'К занятиям в группе приглашаются студенты...')
INSERT INTO Groups VALUES (5, 2, 26, 26, 'закрыта', 'К занятиям в группе приглашаются студенты...')
INSERT INTO Groups VALUES (6, 3, 24, 24, 'закрыта', 'К занятиям в группе приглашаются студенты...')
INSERT INTO Groups VALUES (7, 1, 20, 17, 'открыта', 'К занятиям в группе приглашаются студенты...')

-- User --
INSERT INTO Users VALUES (1, 0, 'Балаганов', 'Модест', 'Аркадьевич', 4, 1, '2019-12-04 16:22:46', '../somepath')
INSERT INTO Users VALUES (2, 0, 'Задов', 'Василий', 'Петрович', 2, 0, '2019-10-16 19:32:55', '../somepath')
INSERT INTO Users VALUES (3, 0, 'Тракторенко', 'Пётр', 'Степанович', 4, 1, '2019-09-05 11:57:02', '../somepath')
INSERT INTO Users VALUES (4, 1, 'Пастушенко', 'Тарас', 'Гаврилович', 0, NULL, NULL, NULL)
INSERT INTO Users VALUES (5, 0, 'Кайло', 'Григорий', 'Кондратьевич', 3, 1, '2019-11-01 17:07:21', '../somepath')
INSERT INTO Users VALUES (6, 0, 'Клюшкина', 'Екатерина', 'Александровна', 1, 1, '2019-09-25 10:07:41', '../somepath')
INSERT INTO Users VALUES (7, 0, 'Кукушкин', 'Вячеслав', 'Леонтьевич', 7, 1, '2019-09-14 15:16:28', '../somepath')

-- Submission --
INSERT INTO Users VALUES (1, 'отмена - Админ', '2019-10-02 19:01:10', '2019-10-03 11:59:14', 2, 2)
INSERT INTO Users VALUES (2, 'активна', '2019-10-03 15:23:16', '2019-10-05 17:53:25', 4, 2)
INSERT INTO Users VALUES (3, 'отмена - ЛК', '2019-10-07 14:19:32', '2019-10-15 17:21:57', 1, 5)
INSERT INTO Users VALUES (4, 'активна', '2019-10-30 10:18:37', '2019-10-30 10:18:37', 2, 4)
INSERT INTO Users VALUES (5, 'активна', '2019-11-06 20:18:55', '2019-11-06 20:18:55', 1, 5)
INSERT INTO Users VALUES (6, 'отмена - ЛК', '2018-12-17 08:17:38', '2019-01-16 20:48:09', 3, 5)
INSERT INTO Users VALUES (7, 'активна', '2019-10-26 14:08:44', '2019-11-01 06:14:37', 7, 7)




