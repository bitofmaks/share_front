-- Role --
CREATE TABLE Roles (
	role_id SMALLINT PRIMARY KEY,
	name VARCHAR(30) DEFAULT NULL
);

-- Health category --
CREATE TABLE Health_Categories (
	category_id SMALLINT PRIMARY KEY,
	name VARCHAR(30) DEFAULT NULL,
	state SMALLINT NOT NULL,
	last_update TIMESTAMP 
);

-- Specialization --
CREATE SEQUENCE specializationIdSequence;  
CREATE TABLE Specializations (
	specialization_id SMALLINT NOT NULL DEFAULT nextval('specializationIdSequence') PRIMARY KEY,
	name VARCHAR(50) DEFAULT NULL,	
	health_category_required SMALLINT REFERENCES health_categories (category_id),
	photo_file_path VARCHAR(50) DEFAULT NULL,
	CONSTRAINT specialization_ibfk_1 FOREIGN KEY (category_id) REFERENCES health_categories (category_id)
);

-- Trainer --
CREATE SEQUENCE trainerIdSequence; 
CREATE TABLE Trainers (
	trainer_id SMALLINT NOT NULL DEFAULT nextval('trainerIdSequence') PRIMARY KEY,
	last_name VARCHAR(30) DEFAULT NULL,
	first_name VARCHAR(30) DEFAULT NULL,
	second_name VARCHAR(30) DEFAULT NULL,	
	specialization_id SMALLINT REFERENCES specializations (specialization_id),
	photo_file_path VARCHAR(50) DEFAULT NULL,
	CONSTRAINT trainers_ibfk_1 FOREIGN KEY (specialization_id) REFERENCES specializations (specialization_id)
);

-- Group schedule --
CREATE SEQUENCE scheduleIdSequence; 
CREATE TABLE Schedules (
	schedule_id SMALLINT DEFAULT nextval('scheduleIdSequence') PRIMARY KEY,
	week SMALLINT NOT NULL,
	day SMALLINT NOT NULL,
	start_at TIME,
	finish_at TIME
);

-- Location --
CREATE SEQUENCE locationIdSequence; 
CREATE TABLE Locations (
	location_id SMALLINT NOT NULL DEFAULT nextval('locationIdSequence') PRIMARY KEY,
	address VARCHAR(255) DEFAULT NULL,
	photo_file_path VARCHAR(50) DEFAULT NULL	
);

-- Group --
CREATE SEQUENCE groupIdSequence;
CREATE TABLE Groups (
	group_id BIGINT NOT NULL DEFAULT nextval('groupIdSequence') PRIMARY KEY,
	group_specialization_id SMALLINT REFERENCES specializations (specialization_id),
	target_size SMALLINT NOT NULL,
	actual_size SMALLINT NOT NULL,
	registration_status VARCHAR(20) DEFAULT NULL,
	description VARCHAR(255) DEFAULT NULL,
	CONSTRAINT group_ibfk_1 FOREIGN KEY (specialization_id) REFERENCES specializations (specialization_id)
);

-- User --
CREATE SEQUENCE userIdSequence;
CREATE TABLE Users (
	user_id BIGINT NOT NULL DEFAULT nextval('userIdSequence') PRIMARY KEY,
	user_role_id SMALLINT REFERENCES roles(role_id),
	last_name VARCHAR(30) DEFAULT NULL,
	first_name VARCHAR(30) DEFAULT NULL,
	second_name VARCHAR(30) DEFAULT NULL,	
	user_group_id BIGINT REFERENCES groups (group_id),
	user_health_category SMALLINT REFERENCES health_categories (category_id),
	confirmed_on TIMESTAMP,
	photo_file_path VARCHAR(50) DEFAULT NULL,
	CONSTRAINT users_ibfk_1 FOREIGN KEY (role_id) REFERENCES roles (role_id),
	CONSTRAINT users_ibfk_2 FOREIGN KEY (category_id) REFERENCES health_categorues (category_id),
	CONSTRAINT users_ibfk_3 FOREIGN KEY (group_id) REFERENCES groups (group_id)
);

-- Submission --
CREATE SEQUENCE submissionIdSequence;
CREATE TABLE Submissions (
	submission_id BIGINT NOT NULL DEFAULT nextval('submissionIdSequence') PRIMARY KEY,
	status VARCHAR(30) DEFAULT NULL, 
	accepted TIMESTAMP, 
	last_update TIMESTAMP, 
	user_id BIGINT REFERENCES users (user_id),
	confirmed_group_id BIGINT REFERENCES groups (group_id),
	CONSTRAINT submission_ibfk_1 FOREIGN KEY (group_id) REFERENCES groups (group_id),
	CONSTRAINT submission_ibfk_2 FOREIGN KEY (user_id) REFERENCES users (user_id)
);







