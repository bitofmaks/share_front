package com.sportsapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.EventListener;
import org.springframework.boot.context.event.ApplicationReadyEvent;

import com.sportsapp.service.TrainerService;

@SpringBootApplication
public class DatabaseComponentsApplication {

	@Autowired
	private TrainerService trainerService;
	
	public static void main(String[] args) {
		SpringApplication.run(DatabaseComponentsApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	private void testDB() {
		//test extract  
	}
}
