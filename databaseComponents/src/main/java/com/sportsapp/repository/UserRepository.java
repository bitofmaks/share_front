package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
