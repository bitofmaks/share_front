package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.HealthCategory;

@Repository
public interface HealthCategoryRepository extends CrudRepository<HealthCategory, Short> {

}
