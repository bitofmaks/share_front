package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.Submission;

@Repository
public interface SubmissionRepository extends CrudRepository<Submission, Long> {

}
