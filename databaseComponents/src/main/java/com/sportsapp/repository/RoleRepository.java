package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Short> {

}
