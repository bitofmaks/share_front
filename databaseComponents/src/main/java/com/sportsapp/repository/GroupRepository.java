package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.Group;

@Repository
public interface GroupRepository extends CrudRepository<Group, Long> {

}
