package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.Location;

@Repository
public interface LocationRepository extends CrudRepository<Location, Short>{

}
