package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.Trainer;
import java.util.List;

@Repository
public interface TrainerRepository extends CrudRepository<Trainer, Short>{
	List<Trainer> findByFirstName(String firstName);
	List<Trainer> findByLastName(String lastName);
}
