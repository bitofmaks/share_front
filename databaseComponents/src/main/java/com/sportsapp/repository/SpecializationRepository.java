package com.sportsapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sportsapp.entity.Specialization;

@Repository
public interface SpecializationRepository extends CrudRepository<Specialization, Short> {

}
