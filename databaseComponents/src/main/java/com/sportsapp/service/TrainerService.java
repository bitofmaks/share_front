package com.sportsapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sportsapp.entity.Trainer;
import com.sportsapp.repository.TrainerRepository;

@Service
public class TrainerService {

	@Autowired
	private final TrainerRepository trainerRepository;
	
	public TrainerService(TrainerRepository trainerRepository) {
		this.trainerRepository = trainerRepository;
	}
	
	public List<Trainer> findByFirstName(String firstName) {
		return trainerRepository.findByFirstName(firstName);
	}
}
