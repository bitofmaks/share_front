package com.sportsapp.entity;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Locations")
@Getter @Setter @NoArgsConstructor 
public class Location {

	@Id
	@SequenceGenerator(name = "locationIdSequence", sequenceName = "locationIdSequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "locationIdSequence")
	@Column(name = "location_id")
	private Short location_id;
	
	@Column(name="address")
	private String address;
	
	@Column(name="photo_file_path")
	private String photoFilePath; 	
	
	@ManyToMany(mappedBy = "locationList")
	private List<Group> groupList;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((location_id == null) ? 0 : location_id.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((photoFilePath == null) ? 0 : photoFilePath.hashCode());
		result = prime * result + ((groupList == null) ? 0 : groupList.hashCode());
				
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if(location_id == null) {
			if(other.location_id != null)
				return false;
		} else if(!location_id.equals(other.location_id))
			return false;
		if(address == null) {
			if(other.address != null)
				return false;
		} else if(!address.equals(other.address))
			return false;
		if(photoFilePath == null) {
			if(other.photoFilePath != null)
				return false;
		} else if(!photoFilePath.equals(other.photoFilePath))
			return false;
		if(groupList == null) {
			if(other.groupList != null)
				return false;
		} else if(!groupList.equals(other.groupList))
			return false;
				
		return true;				
	}
	
	@Override
	public String toString() {
		return "Classes location [id=" + location_id + ", address=" + address + ", active for groups =" + Arrays.toString(groupList.toArray()) + "]"; 
	}
}
