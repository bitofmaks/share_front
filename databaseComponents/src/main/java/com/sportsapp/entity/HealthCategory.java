package com.sportsapp.entity;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Health_categories")
@Getter @Setter @NoArgsConstructor 
public class HealthCategory {
	@Id
	@Column(name = "category_id")
	private Short category_id;
	
	@Column(name="name")
	private String name; 
	
	@Column(name="state")
	private Short state;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = CascadeType.ALL)
	private List <Specialization> specialization;	
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name="last_update")
	private Date last_update;	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "category", cascade = CascadeType.ALL)
	private List <User> userList;	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category_id == null) ? 0 : category_id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
				
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		HealthCategory other = (HealthCategory) obj;
		if(category_id == null) {
			if(other.category_id != null)
				return false;
		} else if(!category_id.equals(other.category_id))
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(state == null) {
			if(other.state != null)
				return false;
		} else if(!state.equals(other.state))
			return false;
		if(last_update == null) {
			if(other.last_update != null)
				return false;
		} else if(!last_update.equals(other.last_update))
			return false;
		
		return true;				
	}
	
	@Override
	public String toString() {
		return "Health category [id=" + category_id + ", name=" + name + ", state=" + state + ", last update on=" + last_update + "]"; 
	}
}
