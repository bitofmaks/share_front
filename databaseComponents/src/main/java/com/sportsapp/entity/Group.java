package com.sportsapp.entity;

import java.util.Arrays;
import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "Groups")
@Getter @Setter @NoArgsConstructor 
public class Group {
	
	@Id
	@SequenceGenerator(name = "groupIdSequence", sequenceName = "groupIdSequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "groupIdSequence")
	@Column(name = "group_id")
	private Long group_id;	
	
	// eventual values: "open", "close"   
	@Column(name="registration_status")
	private String registrationStatus;
	
	// admission quota    
	@Column(name="target_size")
	private Integer targetSize;
	
	// real group size evaluation at the moment  
	@Column(name="actual_size")
	private Integer actualSize;	
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "group", cascade = CascadeType.ALL)
	private List <User> userList;
	
	@ManyToOne
	@JoinColumn(name="group_specialization_id", referencedColumnName = "specialization_id", nullable = false, unique = true)
	private Specialization specialization;
	
	
	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(
		name="schedules",
		joinColumns = { @JoinColumn(name = "group_id") },
		inverseJoinColumns = { @JoinColumn(name="schedule_id", nullable = false) })		
	private List<GroupSchedule> scheduleList;
	
	@ManyToMany(mappedBy = "groupList")
	private List<Trainer> trainerList;
	
	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(
		name="locations",
		joinColumns = { @JoinColumn(name = "group_id", nullable = false) },
		inverseJoinColumns = { @JoinColumn(name="location_id", nullable = false) })
    private List<Location> locationList;
	
	@Column(name="description")
	private String desciption; 
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((group_id == null) ? 0 : group_id.hashCode());
		result = prime * result + ((registrationStatus == null) ? 0 : registrationStatus.hashCode());
		result = prime * result + ((targetSize == null) ? 0 : targetSize.hashCode());
		result = prime * result + ((actualSize == null) ? 0 : actualSize.hashCode());
		result = prime * result + ((specialization == null) ? 0 : specialization.hashCode());
		result = prime * result + ((userList == null) ? 0 : userList.hashCode());
		result = prime * result + ((trainerList == null) ? 0 : trainerList.hashCode());	
		result = prime * result + ((locationList == null) ? 0 : locationList.hashCode());
		result = prime * result + ((scheduleList == null) ? 0 : scheduleList.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if(group_id == null) {
			if(other.group_id != null)
				return false;
		} else if(!group_id.equals(other.group_id))
			return false;
		if(registrationStatus == null) {
			if(other.registrationStatus != null)
				return false;
		} else if(!registrationStatus.equals(other.registrationStatus))
			return false;
		if(targetSize == null) {
			if(other.targetSize != null)
				return false;
		} else if(!targetSize.equals(other.targetSize))
			return false;
		if(actualSize == null) {
			if(other.actualSize != null)
				return false;
		} else if(!actualSize.equals(other.actualSize))
			return false;
		if(specialization == null) {
			if(other.specialization != null)
				return false;
		} else if(!specialization.equals(other.specialization))			
			return false;
		if(userList == null) {
			if(other.userList != null)
				return false;
		} else if(!userList.equals(other.userList))			
			return false;		
		if(trainerList == null) {
			if(other.trainerList != null)
				return false;
		} else if(!trainerList.equals(other.trainerList))
			return false;
		if(locationList == null) {
			if(other.locationList != null)
				return false;
		} else if(!locationList.equals(other.locationList))
			return false;
		if(scheduleList == null) {
			if(other.scheduleList != null)
				return false;
		} else if(!scheduleList.equals(other.scheduleList))
			return false;
		
		return true;				
	}
	
	@Override
	public String toString() {
		return "Group [id=" + group_id + ", specialization ID=" + specialization.getSpecialization_id() + ", registration is open=" + registrationStatus +
				", group target size=" + targetSize + ", group actual size=" + actualSize + 
				", applicants=" + Arrays.toString(userList.toArray()) +				
				", trainers=" + Arrays.toString(trainerList.toArray()) + 
				", locations=" + Arrays.toString(locationList.toArray()) + 
				", schedule=" + Arrays.toString(scheduleList.toArray()) + "]"; 
	}
	
	
		
}
