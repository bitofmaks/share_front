package com.sportsapp.entity;

import java.util.Arrays;
import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Trainers")
@Getter @Setter @NoArgsConstructor 
public class Trainer {
	
	@Id
	@SequenceGenerator(name = "trainerIdSequence", sequenceName = "trainerIdSequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trainerIdSequence")
	@Column(name = "trainer_id")
	private Short trainer_id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="second_name")
	private String secondName;
	
	@Column(name="last_name)")
	private String lastName;
	
	@Column(name="photo_file_path")
	private String photoFilePath; 	
	
	@ManyToOne
	@JoinColumn(name="specialization_id", referencedColumnName = "specialization_id", nullable = false, unique = true)
	private Specialization specialization;
	
	@ManyToMany(cascade =  {CascadeType.ALL})
	@JoinTable(
		name="groups",
		joinColumns = { @JoinColumn(name = "trainer_id", nullable = false) },
		inverseJoinColumns = { @JoinColumn(name="group_id", nullable = false) })
	private List<Group> groupList;	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((trainer_id == null) ? 0 : trainer_id.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((secondName == null) ? 0 : secondName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((photoFilePath == null) ? 0 : photoFilePath.hashCode());
		result = prime * result + ((specialization == null) ? 0 : specialization.hashCode());
		result = prime * result + ((groupList == null) ? 0 : groupList.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Trainer other = (Trainer) obj;
		if(trainer_id == null) {
			if(other.trainer_id != null)
				return false;
		} else if(!trainer_id.equals(other.trainer_id))
			return false;
		if(firstName == null) {
			if(other.firstName != null)
				return false;
		} else if(!firstName.equals(other.firstName))
			return false;
		if(secondName == null) {
			if(other.secondName != null)
				return false;
		} else if(!secondName.equals(other.secondName))
			return false;
		if(lastName == null) {
			if(other.lastName != null)
				return false;
		} else if(!lastName.equals(other.lastName))
			return false;
		if(specialization == null) {
			if(other.specialization != null)
				return false;
		} else if(!specialization.equals(other.specialization))
			return false;
		if(photoFilePath == null) {
			if(other.photoFilePath != null)
				return false;
		} else if(!photoFilePath.equals(other.photoFilePath))
			return false;
		if(groupList == null) {
			if(other.groupList != null)
				return false;
		} else if(!groupList.equals(other.groupList))
			return false;
		
		return true;				
	}
	
	@Override
	public String toString() {
		return "Trainer [id=" + trainer_id + ", first name=" + firstName + ", second name =" + secondName + 
				", last name =" + lastName + ", specialization_id=" + specialization.getSpecialization_id() + 
				", groups of concern=" + Arrays.toString(groupList.toArray()) + "]"; 
	}
	
	
	
}
