package com.sportsapp.entity;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "Submissions")
@Getter @Setter @NoArgsConstructor 
public class Submission {
	
	@Id
	@SequenceGenerator(name = "submissionIdSequence", sequenceName = "submissionIdSequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "submissionIdSequence")
	@Column(name = "submission_id")
	private Long submission_id;
	
	// String attribute defines submission status: "active", "canceled by user","canceled by superuser"  
	@Column(name="status")
	private String statusLabel;
	
	// a moment when submission entered database (therefore, automatically accepted)  
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name="accepted")
	private Date accepted;
	
	// a moment when the latest update was completed   
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name="last_update")
	private Date last_update;
	
	@ManyToOne
	@JoinColumn(name="confirmed_group_id")
	private Group group;
	
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName = "user_id", nullable = false, unique = true)
	private User user;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((submission_id == null) ? 0 : submission_id.hashCode());
		result = prime * result + ((accepted == null) ? 0 : accepted.hashCode());
		result = prime * result + ((last_update == null) ? 0 : last_update.hashCode());
		result = prime * result + ((statusLabel == null) ? 0 : statusLabel.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Submission other = (Submission) obj;
		if(submission_id == null) {
			if(other.submission_id != null)
				return false;
		} else if(!submission_id.equals(other.submission_id))
			return false;
		if(statusLabel == null) {
			if(other.statusLabel != null)
				return false;
		} else if(!statusLabel.equals(other.statusLabel))
			return false;
		if(accepted == null) {
			if(other.accepted != null)
				return false;
		} else if(!accepted.equals(other.accepted))
			return false;
		if(last_update == null) {
			if(other.last_update != null)
				return false;
		} else if(!last_update.equals(other.last_update))
			return false;
		if(group == null) {
			if(other.group != null)
				return false;
		} else if(!group.equals(other.group))
			return false;
		if(user == null) {
			if(other.user != null)
				return false;
		} else if(!user.equals(other.user))
			return false;
				
		return true;				
	}
	@Override
	public String toString() {
		return "Submission [id=" + submission_id + ", user ID=" + user.getUser_id() + ", group ID=" + group.getGroup_id() +
				", status=" + statusLabel + ", registered on=" + accepted + 
				", last update on=" + last_update + "]"; 
	}
	
}
