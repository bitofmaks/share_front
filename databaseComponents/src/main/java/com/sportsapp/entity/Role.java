package com.sportsapp.entity;

import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Roles")
@Getter @Setter @NoArgsConstructor 
public class Role {
	
	@Id
	@Column(name = "role_id")
	private Short role_id;
	
	@Column(name="name")
	private String name; 	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "role", cascade = CascadeType.ALL)
	List <User> userList;	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((role_id == null) ? 0 : role_id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
				
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if(role_id == null) {
			if(other.role_id != null)
				return false;
		} else if(!role_id.equals(other.role_id))
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		
		return true;				
	}
	
	@Override
	public String toString() {
		return "Role [id=" + role_id + ", name=" + name + "]"; 
	}
	
	
}
