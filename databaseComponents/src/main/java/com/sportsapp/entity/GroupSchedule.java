/** NOTE: 
 * means the same as "group_timetable" at https://app.dbdesigner.net/designer/schema/0-trpo-016e7e20-38f8-47bd-8958-0171119c7a7b
**/
package com.sportsapp.entity;

import java.util.Arrays;
import java.util.List;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Schedules")
@Getter @Setter @NoArgsConstructor 
public class GroupSchedule {
	
	@Id
	@SequenceGenerator(name = "scheduleIdSequence", sequenceName = "scheduleIdSequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scheduleIdSequence")
	@Column(name = "schedule_id")
	private Short schedule_id;
	
	@Column(name="week")
	private Short week;
	
	@Column(name="day")
	private Short day;
	
	@Column(name="start_at")
	@CreationTimestamp
	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern = "HH:mm")
	private Date timeToBegin;
	
	@Column(name="finish_at")
	@CreationTimestamp
	@Temporal(TemporalType.TIME)
	@JsonFormat(pattern = "HH:mm")
	private Date timeToEnd;	
	
	@ManyToMany(mappedBy = "scheduleList")
	private List <Group> groupList;	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((schedule_id == null) ? 0 : schedule_id.hashCode());
		result = prime * result + ((week == null) ? 0 : week.hashCode());
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((timeToBegin == null) ? 0 : timeToBegin.hashCode());
		result = prime * result + ((timeToEnd == null) ? 0 : timeToEnd.hashCode());
		result = prime * result + ((groupList == null) ? 0 : groupList.hashCode());
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		GroupSchedule other = (GroupSchedule) obj;
		if(schedule_id == null) {
			if(other.schedule_id != null)
				return false;
		} else if(!schedule_id.equals(other.schedule_id))
			return false;
		if(week == null) {
			if(other.week != null)
				return false;
		} else if(!week.equals(other.week))
			return false;
		if(day == null) {
			if(other.day != null)
				return false;
		} else if(!day.equals(other.day))
			return false;
		if(timeToBegin == null) {
			if(other.timeToBegin != null)
				return false;
		} else if(!timeToBegin.equals(other.timeToBegin))
			return false;
		if(timeToEnd == null) {
			if(other.timeToEnd != null)
				return false;
		} else if(!timeToEnd.equals(other.timeToEnd))
			return false;
		if(groupList == null) {
			if(other.groupList != null)
				return false;
		} else if(!groupList.equals(other.groupList))
			return false;
		
		return true;				
	}
	
	@Override
	public String toString() {
		return "Group schedule [id=" + schedule_id + ", week=" + week + ", day=" + day +  ", lesson begins at=" + timeToBegin + 
				", lesson finishes at=" + timeToEnd + ", active for groups=" + Arrays.toString(groupList.toArray()) + "]"; 
	}
	
	
	
}
