package com.sportsapp.entity;

import java.util.List;

import javax.persistence.*;

import java.sql.Timestamp;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Users")
@Getter @Setter @NoArgsConstructor 
public class User {
	
	@Id
	@SequenceGenerator(name = "userIdSequence", sequenceName = "userIdSequence", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userIdSequence")
	@Column(name = "user_id")
	private Long user_id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="second_name")
	private String secondName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="photo_file_path")
	private String photoFilePath;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
	private List <Submission> submissions;	
	
	@ManyToOne
	@JoinColumn(name="user_role_id", referencedColumnName = "role_id", nullable = false, unique = true)
	private Role role;
	
	@ManyToOne
	@JoinColumn(name="user_group_id", referencedColumnName = "group_id", nullable = false, unique = true)
	private Group group;
	
	@ManyToOne
	@JoinColumn(name="user_health_category", referencedColumnName = "category_id", nullable = false, unique = true)
	private HealthCategory category;
	
	@Column(name="confirmed_on")
	private Timestamp confirmed;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((secondName == null) ? 0 : secondName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((photoFilePath == null) ? 0 : photoFilePath.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((confirmed == null) ? 0 : confirmed.hashCode());
				
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) 
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if(user_id == null) {
			if(other.user_id != null)
				return false;
		} else if(!user_id.equals(other.user_id))
			return false;
		if(firstName == null) {
			if(other.firstName != null)
				return false;
		} else if(!firstName.equals(other.firstName))
			return false;
		if(secondName == null) {
			if(other.secondName != null)
				return false;
		} else if(!secondName.equals(other.secondName))
			return false;
		if(lastName == null) {
			if(other.lastName != null)
				return false;
		} else if(!lastName.equals(other.lastName))
			return false;
		if(role == null) {
			if(other.role != null)
				return false;
		} else if(!role.equals(other.role))
			return false;
		if(photoFilePath == null) {
			if(other.photoFilePath != null)
				return false;
		} else if(!photoFilePath.equals(other.photoFilePath))
			return false;
		if(group == null) {
			if(other.group != null)
				return false;
		} else if(!group.equals(other.group))
			return false;
		if(category == null) {
			if(other.category != null)
				return false;
		} else if(!category.equals(other.category))
			return false;
		if(confirmed == null) {
			if(other.confirmed != null)
				return false;
		} else if(!confirmed.equals(other.confirmed))
			return false;
			
		return true;				
	}
	
	@Override
	public String toString() {
		return "User [id=" + user_id + ", role_id =" + role.getRole_id() +", first name=" + firstName + ", second name =" + secondName + 
				", last name =" + lastName + ", group =" + group.getGroup_id() + 
				", health category =" + category.getCategory_id() + ", confirmed on =" + confirmed + "]"; 
	}
	
	
}